#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<ll, ll> ii;
typedef vector<ll> vi;
typedef vector<ii> vii;
typedef vector<vi> vvi;

const double EPS = 1e-9;
const ll INF = 1e9;

bool equals(double a, double b)
{
    return abs(a - b) < EPS;
}
int main()
{
    ios::sync_with_stdio(0);
    cin.tie(0);
}
