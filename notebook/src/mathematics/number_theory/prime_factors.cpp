// Based on code by Halim (p. 238, 239, 240).

#include "eratosthenes.cpp"

// Given a list of primes in range [2, sqrt n], returns the prime
// factors of n (base, exponent). Time: O(sqrt n / log sqrt n).
vii factors(ll n)
{
    vii fac;
    for (ll i = 0; i < (ll)pr.size() && pr[i] * pr[i] <= n; i++)
    {
        ll d = pr[i], e = 0;
        for (; n % d == 0; n /= d, e++);
        if (e)
            fac.push_back({d, e});
    }
    if (n > 1)
        fac.push_back({n, 1});
    return fac;
}

// Given n prime factors (with exponents) of m, returns the number of
// divisors of m. Time: O(n).
ll divisors(vii fac)
{
    ll d = 1;
    for (auto p: fac)
        d *= p.second + 1;
    return d;
}

// Given n prime factors (with exponents) of m, returns the number of
// positive integers < m that are relatively prime to m. Time: O(n).
ll euler_phi(ll m, vii fac)
{
    ll phi = m;
    for (auto p: fac)
        phi -= phi / p.first;
    return phi;
}
