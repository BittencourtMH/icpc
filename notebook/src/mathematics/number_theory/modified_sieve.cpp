// Based on code by Halim (p. 241).

#include "../../template.cpp"

const ll N = 10000000; // maximum number

ll fac[N + 1]; // number of different prime factors of i

// Stores number of different prime factors in range [2, n]. Time:
// O(n log log n).
void modified_sieve(ll n)
{
    for (ll i = 2; i <= n; i++)
        if (!fac[i])
            for (ll j = i; j <= n; j += i)
                fac[j]++;
}
