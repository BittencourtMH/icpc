// Based on code by Halim (p. 391).

#include "../../template.cpp"

// Returns b ^ e. Time: O(log e).
ll power(ll b, ll e)
{
    ll p = 1, q = b;
    for (; e; e >>= 1)
    {
        if (e & 1)
            p *= q;
        q *= q;
    }
    return p;
}
