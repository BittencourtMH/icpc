// Based on code by Halim (p. 242).

#include "../../template.cpp"

ll x, y; // first solution
ll d; // auxiliary variable

// Finds the first solution for linear diophantine equation
// ax + by = c. Time: O(log max(a, b)).
void extended_euclid(ll a, ll b)
{
    if (!b)
    {
        x = 1;
        y = 0;
        d = a;
        return;
    }
    extended_euclid(b, a % b);
    ll y1 = x - (a / b) * y;
    x = y;
    y = y1;
}
