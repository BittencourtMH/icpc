// Based on code by...

#include "../../template.cpp"

struct rational
{
    ll n, d; // numerator and denominator

    // Returns the sum (this + r). Time: O(log min(n, d)).
    rational operator+(rational r)
    {
        return rational{n * r.d + r.n * d, d * r.d}.simplify();
    }

    // Returns the difference (this - r). Time: O(log min(n, d)).
    rational operator-(rational r)
    {
        return rational{n * r.d - r.n * d, d * r.d}.simplify();
    }

    // Returns the product (this * r). Time: O(log min(n, d)).
    rational operator*(rational r)
    {
        return rational{n * r.n, d * r.d}.simplify();
    }

    // Returns the quotient (this / r). Time: O(log min(n, d)).
    rational operator/(rational r)
    {
        return rational{n * r.d, d * r.n}.simplify();
    }

    // Returns the simplified rational number. Time:
    // O(log min(n, d)).
    rational simplify()
    {
        ll gcd = __gcd(n, d);
        return {n / gcd, d / gcd};
    }
};
