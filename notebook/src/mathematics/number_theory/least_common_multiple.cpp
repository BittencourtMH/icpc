// Based on code by Halim (p. 237).

#include "../../template.cpp"

// Returns least common multiple of a and b. Time: O(log max(a, b)).
ll lcm(ll a, ll b)
{
    return a * (b / __gcd(a, b));
}
