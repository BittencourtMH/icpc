// Based on code by Halim (p. 236).

#include "../../template.cpp"

const ll N = 10000000; // maximum number

bool p[N + 1]; // true if i is prime
vi pr; // list of primes

// Stores primes in range [2, n]. Time: O(n log log n).
void eratosthenes(ll n)
{
    fill(p + 2, p + n + 1, 1);
    for (ll i = 2; i <= n; i++)
        if (p[i])
        {
            for (ll j = i * i; j <= n; j += i)
                p[j] = 0;
            pr.push_back(i);
        }
}

// Given a list of primes in range [2, n], returns true if m is
// prime. Constraint: m <= n ^ 2. Time: O(sqrt m / log sqrt m).
bool prime(ll n, ll m)
{
    if (m <= n)
        return p[m];
    for (ll i = 0; i < (ll)pr.size() && pr[i] * pr[i] <= m; i++)
        if (m % pr[i] == 0)
            return 0;
    return 1;
}
