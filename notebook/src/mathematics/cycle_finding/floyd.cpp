// Based on code by Halim (p. 250).

#include "../../template.cpp"

// Function (ad-hoc code).
ll f(ll x)
{
    return (3 * x + 1) % 4;
}

// Given an initial value x0, returns
// {mu, lambda} = {start of cycle, cycle length}. Time:
// O(mu + lambda).
ii floyd(ll x0)
{
    ll tortoise = f(x0), hare = f(tortoise), mu = 0, lambda = 1;
    while (tortoise != hare)
        tortoise = f(tortoise), hare = f(f(hare));
    hare = x0;
    while (tortoise != hare)
        tortoise = f(tortoise), hare = f(hare), mu++;
    hare = f(tortoise);
    while (tortoise != hare)
        hare = f(hare), lambda++;
    return {mu, lambda};
}
