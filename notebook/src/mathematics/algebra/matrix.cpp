// Based on code by Halim (p. 391).

#include "../../template.cpp"

const ll R = 1000, C = 1000; // maximum number of rows and columns

struct matrix
{
    ll r, c; // number of rows and columns
    ll mat[R][C]; // 2D array

    // Returns identity matrix of size n. Time: O(n ^ 2).
    static matrix id(ll n)
    {
        matrix ans;
        ans.r = ans.c = n;
        for (ll i = 0; i < n; i++)
            for (ll j = 0; j < n; j++)
                ans.mat[i][j] = i == j;
        return ans;
    }

    // Given two r x c matrices, returns their sum. Time: O(rc).
    matrix operator+(matrix m)
    {
        matrix ans;
        ans.r = r;
        ans.c = c;
        for (ll i = 0; i < r; i++)
            for (ll j = 0; j < c; j++)
                ans.mat[i][j] = mat[i][j] + m.mat[i][j];
        return ans;
    }

    // Given two matrices m x n and n x p, returns their product
    // m x p. Time: O(mnp).
    matrix operator*(matrix m)
    {
        matrix ans;
        ans.r = r;
        ans.c = m.c;
        for (ll i = 0; i < ans.r; i++)
            for (ll j = 0; j < ans.c; j++)
            {
                ans.mat[i][j] = 0;
                for (ll k = 0; k < c; k++)
                    ans.mat[i][j] += mat[i][k] * m.mat[k][j];
            }
        return ans;
    }

    // Given a matrix n x n and an exponent e, returns the matrix
    // raised to e. Time: O(n ^ 3 log e).
    matrix operator^(ll e)
    {
        matrix p = id(r), q = *this;
        for (; e; e >>= 1)
        {
            if (e & 1)
                p = p * q;
            q = q * q;
        }
        return p;
    }

    // Given a matrix r x c, returns its transpose. Time: O(rc).
    matrix transpose()
    {
        matrix ans;
        ans.r = c;
        ans.c = r;
        for (ll i = 0; i < c; i++)
            for (ll j = 0; j < r; j++)
                ans.mat[i][j] = mat[j][i];
        return ans;
    }
};
