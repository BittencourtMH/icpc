#include "../../template.cpp"

const ll N = 10000000; // number of elements

double v[N], w[N]; // values and weights

// Returns the arithmetic mean of n elements. Time: O(n).
double arithmetic_mean(ll n)
{
    return accumulate(v, v + n, 0LL) / n;
}

// Returns the weighted arithmetic mean of n elements. Time: O(n).
double weighted_mean(ll n)
{
    double sv = 0, sw = 0;
    for (ll i = 0; i < n; i++)
        sv += v[i] * w[i], sw += w[i];
    return sv / sw;
}
