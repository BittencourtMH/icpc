// Based on code by Halim (p. 82-83).

#include "../template.cpp"

const ll N = 1000000; // maximum number of elements

struct segment_tree_lazy
{
    ll a[4 * N]; // implicit tree (normal nodes)
    ll lazy[4 * N]; // implicit tree (lazy nodes)
    ll s; // size of tree

    // Restarts the tree with n elements. Time: O(n).
    void start(ll n)
    {
        s = 1 << (ll)ceil(log2(n + 1));
        fill(a, a + 2 * s, 0);
        fill(lazy, lazy + 2 * s, 0);
    }

    // Returns the sum of elements in range [i, j), accessing index
    // p, which represents range [l, r). Time: O(log s).
    ll get(ll i, ll j, ll p, ll l, ll r)
    {
        if (i == l && j == r)
            return a[p];
        ll m = (l + r) / 2;
        if (j <= m)
            return (j - i) * lazy[p] + get(i, j, 2 * p, l, m);
        if (i >= m)
            return (j - i) * lazy[p] + get(i, j, 2 * p + 1, m, r);
        return (j - i) * lazy[p] + get(i, m, 2 * p, l, m) +
            get(m, j, 2 * p + 1, m, r);
    }

    // Returns the sum of elements in range [i, j]. Time: O(log s).
    ll get(ll i, ll j)
    {
        return get(i, j + 1, 1, 0, s);
    }

    // Adds v to every element in [i, j), accessing index p, which
    // represents range [l, r). Time: O(log s).
    void add(ll i, ll j, ll p, ll l, ll r, ll v)
    {
        if (i == l && j == r)
        {
            lazy[p] += (j - i) * v;
            for (; p; p /= 2)
                a[p] += (j - i) * v;
        }
        else
        {
            ll m = (l + r) / 2;
            if (j <= m)
                add(i, j, 2 * p, l, m, v);
            else if (i >= m)
                add(i, j, 2 * p + 1, m, r, v);
            else
            {
                add(i, m, 2 * p, l, m, v);
                add(m, j, 2 * p + 1, m, r, v);
            }
        }
    }

    // Adds v to every element in [i, j]. Time: O(log s).
    void add(ll i, ll j, ll v)
    {
        add(i, j + 1, 1, 0, s, v);
    }
};
