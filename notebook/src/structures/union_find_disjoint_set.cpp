// Based on code by Halim (p. 79).

#include "../template.cpp"

const ll N = 10000000; // maximum number of vertices

struct ufds
{
    ll p[N]; // parent of vertex i
    ll r[N]; // rank of vertex i

    // Restarts the forest with n vertices. Time: O(n).
    void start(ll n)
    {
        for (ll i = 0; i < n; i++)
            p[i] = i, r[i] = 0;
    }

    // Merges trees containing i and j. Time: O(1).
    void merge(ll i, ll j)
    {
        i = find(i);
        j = find(j);
        if (i == j)
            return;
        if (r[i] > r[j])
            p[j] = i;
        else if (r[i] < r[j])
            p[i] = j;
        else
            p[j] = i, r[i]++;
    }

    // Returns the root of the tree containing i. Time: O(1).
    ll find(ll i)
    {
        return p[i] == i ? i : p[i] = find(p[i]);
    }
};
