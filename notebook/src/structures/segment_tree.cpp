// Based on code by Halim (p. 82-83).

#include "../template.cpp"

const ll N = 1000000; // maximum number of elements

struct segment_tree
{
    ll a[4 * N]; // implicit tree
    ll s; // size of tree

    // Restarts the tree with n elements. Time: O(n).
    void start(ll n)
    {
        s = 1 << (ll)ceil(log2(n + 1));
        fill(a, a + 2 * s, 0);
    }

    // Returns the sum of elements in range [i, j), accessing index
    // p, which represents range [l, r). Time: O(log s).
    ll get(ll i, ll j, ll p, ll l, ll r)
    {
        if (i == l && j == r)
            return a[p];
        ll m = (l + r) / 2;
        if (j <= m)
            return get(i, j, 2 * p, l, m);
        if (i >= m)
            return get(i, j, 2 * p + 1, m, r);
        return get(i, m, 2 * p, l, m) + get(m, j, 2 * p + 1, m, r);
    }

    // Returns the sum of elements in range [i, j]. Time: O(log s).
    ll get(ll i, ll j)
    {
        return get(i, j + 1, 1, 0, s);
    }

    // Sets element i to value v. Time: O(log s).
    void set(ll i, ll v)
    {
        ll p = s + i, d = v - a[p];
        for (; p; a[p] += d, p /= 2);
    }
};
