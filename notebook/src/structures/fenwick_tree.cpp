// Based on code by Halim (p. 86-87).

#include "../template.cpp"

const ll N = 1000000; // maximum number of elements

struct fenwick_tree
{
    ll a[N + 1]; // implicit tree (1-indexed)
    ll s; // size of tree

    // Restarts the tree with n elements. Time: O(n).
    void start(ll n)
    {
        s = n + 1;
        fill(a, a + s, 0);
    }

    // Returns the sum of elements in range [i, j]. Time: O(log s).
    ll get(ll i, ll j)
    {
        if (i != 1)
            return get(1, j) - get(1, i - 1);
        ll sum = 0;
        for (; j; sum += a[j], j -= j & -j);
        return sum;
    }

    // Adds v to element i. Time: O(log s).
    void add(ll i, ll v)
    {
        for (; i < s; a[i] += v, i += i & -i);
    }
};
