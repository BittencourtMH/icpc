// Based on code by Halim (p. 129).

#include "../../template.cpp"

const ll N = 100000000; // maximum size of array

ll a[N]; // array

// Returns the sum of the maximum subarray in range [l, r] of a.
// Time: O(r - l).
ll kadane(ll l, ll r)
{
    ll c = a[l], g = a[l];
    for (ll i = l + 1; i <= r; i++)
    {
        c = max(c + a[i], a[i]);
        g = max(g, c);
    }
    return g;
}
