// Based on code by Halim (p. 130).

#include "../../template.cpp"

const ll R = 100, C = 100; // maximum number of rows and columns

ll m[R][C]; // matrix
ll ac[R][C]; // accumulated sum of submatrix (0, 0) to (i, j).

// Returns the sum of the maximum submatrix of the first r rows and c
// columns of m. Time: O((rc) ^ 2).
ll max_submatrix(ll r, ll c)
{
    for (ll i = 0; i < r; i++)
        for (ll j = 0; j < c; j++)
        {
            ac[i][j] = m[i][j];
            if (i)
                ac[i][j] += ac[i - 1][j];
            if (j)
                ac[i][j] += ac[i][j - 1];
            if (i && j)
                ac[i][j] -= ac[i - 1][j - 1];
        }
    ll best = ac[0][0];
    for (ll i = 0; i < r; i++)
        for (ll j = 0; j < c; j++)
            for (ll k = i; k < r; k++)
                for (ll l = j; l < c; l++)
                {
                    ll sum = ac[k][l];
                    if (i)
                        sum -= ac[i - 1][l];
                    if (j)
                        sum -= ac[k][j - 1];
                    if (i && j)
                        sum += ac[i - 1][j - 1];
                    best = max(best, sum);
                }
    return best;
}
