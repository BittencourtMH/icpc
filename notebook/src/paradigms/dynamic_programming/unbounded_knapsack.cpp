// Based on code by Halim (p. 132-133).

#include "../../template.cpp"

const ll N = 10000; // maximum number of objects
const ll C = 10000; // maximum knapsack capacity

ll v[N + 1]; // value of objects (1-indexed)
ll w[N + 1]; // weight of objects (1-indexed)
ll memo[C + 1]; // memoization table

// Given n objects and a knapsack with capacity c, returns the
// maximum value the knapsack can carry. Time: O(nc).
ll unbounded_knapsack(ll n, ll c)
{
    memo[0] = 0;
    for (ll i = 1; i <= c; i++)
    {
        memo[i] = 0;
        for (ll j = 1; j <= n; j++)
            if (w[j] <= i)
                memo[i] = max(memo[i], memo[i - w[j]] + v[j]);
    }
    return memo[c];
}
