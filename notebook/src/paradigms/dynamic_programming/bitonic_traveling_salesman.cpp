// Based on code by Halim (p. 365).

#include "../../template.cpp"

const ll N = 10000; // maximum number of vertices

ll d[N][N], memo[N][N]; // distance matrix and memoization table
ll n; // number of vertices

// Returns the length of path from u to v. Time: O(n ^ 2).
double btsp(ll u, ll v)
{
    if (memo[u][v] != -1)
        return memo[u][v];
    ll w = max(u, v) + 1;
    if (w == n - 1)
        return memo[u][v] = d[u][w] + d[w][v];
    return min(d[u][w] + btsp(w, v), d[w][v] + btsp(u, w));
}
