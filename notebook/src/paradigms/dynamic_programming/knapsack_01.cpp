// Based on code by Halim (p. 132-133).

#include "../../template.cpp"

const ll N = 10000; // maximum number of objects
const ll C = 10000; // maximum knapsack capacity

ll v[N + 1]; // value of objects (1-indexed)
ll w[N + 1]; // weight of objects (1-indexed)
ll memo[N + 1][C + 1]; // memoization table

// Given n objects and a knapsack with capacity c, returns the
// maximum value the knapsack can carry. Time: O(nc).
ll knapsack_01(ll n, ll c)
{
    for (ll i = 0; i <= c; i++)
        memo[0][i] = 0;
    for (ll i = 1; i <= n; i++)
    {
        memo[i][0] = 0;
        for (ll j = 1; j <= c; j++)
            if (w[i] <= j)
                memo[i][j] = max(memo[i - 1][j],
                    memo[i - 1][j - w[i]] + v[i]);
            else
                memo[i][j] = memo[i - 1][j];
    }
    return memo[n][c];
}
