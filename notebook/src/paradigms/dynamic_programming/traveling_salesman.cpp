// Based on text by Halim (p. 135-136).

#include "../../template.cpp"

const ll V = 16; // maximum number of vertices

ll g[V][V]; // graph (adjacency matrix)
ll memo[V][1 << V]; // memoization table
ll v; // number of vertices

// Returns the minimum cost to go from p to all remaining vertices in
// m (bitmask) and return to vertice 0. Time: O(v * 2 ^ v).
ll tsp(ll p, ll m)
{
    if (memo[p][m])
        return memo[p][m];
    if (m == (1 << v) - 1)
        return memo[p][m] = g[p][0];
    ll d = INT_MAX;
    for (ll i = 0; i < v; i++)
        if (i != p && !(m & (1 << i)))
            d = min(d, g[p][i] + tsp(i, m | (1 << i)));
    return memo[p][m] = d;
}
