// Based on text by Halim (p. 130-132).

#include "../../template.cpp"

const ll N = 10000000; // maximum size of sequence

ll a[N]; // sequence
ll s[N]; // longest increasing subsequence ending at index i

// Returns the size of the longest increasing subsequence in range
// [l, r] of a. Time: O((r - l) ^ 2).
ll lis(ll l, ll r)
{
    ll g = 0;
    for (ll i = l; i <= r; i++)
    {
        s[i] = 0;
        for (ll j = 0; j < i; j++)
            if (a[j] < a[i])
                s[i] = max(s[i], s[j]);
        s[i]++;
        g = max(g, s[i]);
    }
    return g;
}
