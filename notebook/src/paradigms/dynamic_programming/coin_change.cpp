// Based on text by Halim (p. 133-134).

#include "../../template.cpp"

const ll N = 10000; // maximum number of coin values
const ll V = 10000; // maximum value

ll coin[N]; // coin values
ll memo[V]; // memoization table
ll n; // number of coin values

// Returns the minimum number of coins needed to obtain value v.
// Time: O(nv).
ll coin_change(ll v)
{
    memo[0] = 0;
    for (ll i = 1; i <= v; i++)
    {
        memo[i] = INT_MAX;
        for (ll j = 0; j < n; j++)
            if (v - coin[j] >= 0)
                memo[i] = min(memo[i], memo[v - coin[j]]) + 1;
    }
    return memo[v];
}
