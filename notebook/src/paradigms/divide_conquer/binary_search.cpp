// Based on code by Halim (p. 112).

#include "../../template.cpp"

double b, B, h, drink; // ad-hoc code

// Returns whether the calculated value is below the target,
// given the variable n. Ad-hoc code (URI 1549).
bool test(double n)
{
    double b_part = b + (B - b) * n / h;
    return M_PI * n * (b_part * b_part + b_part * b + b * b) / 3 <
            drink;
}

// Converts n to a string with d digits of precision. Time: O(1).
string format(double n, ll d)
{
    ostringstream oss;
    oss << fixed << setprecision(d) << n;
    return oss.str();
}

// Calculates the answer in range [l, r] with d digits of precision.
// Time: O(log((r - l) * 10 ^ d)).
double binary_search(double l, double r, ll d)
{
    double eps = pow(10, -d);
    while (r - l > eps || format(l, d) != format(r, d))
    {
        double m = (l + r) / 2;
        test(m) ? l = m : r = m;
    }
    return l;
}
