// Based on code by Halim (p. 325).

#include "../../template.cpp"

ll ok; // all queens are placed

// Given initial state (row, left diagonal, right diagonal), returns
// the number of ways n queens can be placed in a n x n chessboard.
// Time: O(n!!).
ll backtrack(ll r, ll ld, ll rd)
{
    if (r == ok)
        return 1;
    ll ans = 0, pos = ok & ~(r | ld | rd);
    while (pos)
    {
        ll p = pos & -pos;
        pos -= p;
        ans += backtrack(r | p, (ld | p) << 1, (rd | p) >> 1);
    }
    return ans;
}

// Returns the number of ways n queens can be placed in a n x n
// chessboard. Time: O(n!!).
ll n_queens(ll n)
{
    ok = (1 << n) - 1;
    return backtrack(0, 0, 0);
}
