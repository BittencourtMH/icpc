// Based on code by Halim (p. 314-315, 316).

#include "polygon.cpp"

point pivot; // starting vertex of convex hull

// Return if p's angle is less than q's angle with respect to pivot.
// Time: O(1).
bool compare_angle(const point &p, const point &q)
{
    if (pivot.collinear(p, q))
        return pivot.distance(p) < pivot.distance(q);
    vector2d v1 = pivot.to_vector(p), v2 = pivot.to_vector(q);
    return atan2(v1.y, v1.x) < atan2(v2.y, v2.x);
}

// Given n points (v), returns the convex hull. Time: O(n log n).
polygon graham(vector<point> &v)
{
    polygon p;
    if (v.size() <= 3)
        p.v = v, p.v.push_back(v[0]);
    else
    {
        sort(v.begin(), v.end());
        pivot = v[0];
        sort(++v.begin(), v.end(), compare_angle);
        p.v.push_back(v.back());
        p.v.push_back(v[0]);
        p.v.push_back(v[1]);
        for (ll i = 2; i < (ll)v.size();)
            if (!p.v[p.v.size() - 2].clockwise(p.v.back(), v[i]))
                p.v.push_back(v[i++]);
            else
                p.v.pop_back();
    }
    return p;
}
