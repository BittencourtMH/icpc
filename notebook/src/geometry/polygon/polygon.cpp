// Based on code by Halim (p. 310, 311, 312, 313).

#include "../objects/line.cpp"

struct polygon
{
    vector<point> v; // vertices (vertex 0 = vertex n - 1)

    // Returns the number of vertices. Time: O(1).
    ll vertices()
    {
        return v.size() - 1;
    }

    // Given n vertices, returns the perimeter. Time: O(n).
    double perimeter()
    {
        double p = 0;
        for (ll i = 0; i < vertices(); i++)
            p += v[i].distance(v[i + 1]);
        return p;
    }

    // Given n vertices, returns the area. Time: O(n).
    double area()
    {
        double a = 0;
        for (ll i = 0; i < vertices(); i++)
            a += v[i].x * v[i + 1].y - v[i].y * v[i + 1].x;
        return a / 2;
    }

    // Given n vertices, returns if the polygon is convex. Time:
    // O(n).
    bool convex()
    {
        bool d = v[0].clockwise(v[1], v[2]);
        for (ll i = 1; i < vertices() - 1; i++)
            if (d != v[i].clockwise(v[i + 1], v[i + 2]))
                return 0;
        if (d != v[vertices() - 1].clockwise(v[0], v[1]))
            return 0;
        return 1;
    }

    // Given n vertices, returns if p is inside the polygon. Time:
    // O(n).
    bool inside(point p)
    {
        double s = 0;
        for (ll i = 0; i < vertices(); i++)
            if (p.clockwise(v[i], v[i + 1]))
                s += p.angle(v[i], v[i + 1]);
            else
                s -= p.angle(v[i], v[i + 1]);
        return equals(abs(s), 2 * M_PI);
    }

    // Given n vertices, cuts polygon with line l. Time: O(n).
    polygon cut(line l)
    {
        polygon p;
        for (ll i = 0; i < (ll)v.size(); i++)
        {
            vector2d v1 = l.p1.to_vector(l.p2);
            vector2d v2 = l.p1.to_vector(v[i]);
            double l1 = v1.cross(v2), l2 = 0;
            if (i != (ll)v.size() - 1)
                l2 = v1.cross(l.p1.to_vector(v[i + 1]));
            if (l1 > -EPS)
                p.v.push_back(v[i]);
            if (l1 * l2 < -EPS)
            {
                line e = from_points(v[i], v[i + 1]);
                p.v.push_back(l.intersection(e));
            }
        }
        if (!p.v.empty() && !(p.v.front() == p.v.back()))
            p.v.push_back(p.v.front());
        return p;
    }
};
