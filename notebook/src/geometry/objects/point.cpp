// Based on code by Halim (p. 296, 297, 299, 300).

#include "vector.cpp"

struct point
{
    double x, y; // 2D coordinates

    // Returns true if both points are equal. Time: O(1).
    bool operator==(const point &p) const
    {
        return equals(x, p.x) && equals(y, p.y);
    }

    // Returns true if this point is less than p. Time: O(1).
    bool operator<(const point &p) const
    {
        if (!equals(x, p.x))
            return x < p.x;
        return y < p.y;
    }

    // Returns distance between points. Time: O(1).
    double distance(point p)
    {
        return hypot(x - p.x, y - p.y);
    }

    // Returns a point translated by (dx, dy). Time: O(1).
    point translate(double dx, double dy)
    {
        return {x + dx, y + dy};
    }

    // Returns a point rotated by a radians counter clockwise with
    // respect to origin. Time: O(1).
    point rotate(double a)
    {
        return {x * cos(a) - y * sin(a), x * sin(a) + y * cos(a)};
    }

    // Returns a vector from this point to p. Time: O(1).
    vector2d to_vector(point p)
    {
        return {p.x - x, p.y - y};
    }

    // Returns if vector (this, q) is on the right side of (this, p).
    // Time: O(1).
    bool clockwise(point p, point q)
    {
        return to_vector(p).clockwise(to_vector(q));
    }

    // Returns if three points are collinear. Time: O(1).
    bool collinear(point p, point q)
    {
        return to_vector(p).collinear(to_vector(q));
    }

    // Returns angle between p and q with respect to this point.
    // Time: O(1).
    double angle(point p, point q)
    {
        return to_vector(p).angle(to_vector(q));
    }
};
