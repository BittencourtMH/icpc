// Based on code by Halim (p. 299, 300).

#include "../../template.cpp"

struct vector2d
{
    double x, y; // 2D coordinates

    // Returns if both vectors are equal. Time: O(1).
    bool operator==(const vector2d &v) const
    {
        return equals(x, v.x) && equals(y, v.y);
    }

    // Returns if this vector is less than v. Time: O(1).
    bool operator<(const vector2d &v) const
    {
        if (!equals(x, v.x))
            return x < v.x;
        return y < v.y;
    }

    // Returns a vector scaled by factor s. Time: O(1).
    vector2d scale(double s)
    {
        return {x * s, y * s};
    }

    // Returns dot product of both vectors. Time: O(1).
    double dot(vector2d v)
    {
        return x * v.x + y * v.y;
    }

    // Returns cross product of both vectors. Time: O(1).
    double cross(vector2d v)
    {
        return x * v.y - y * v.x;
    }

    // Returns norm of vector. Time: O(1).
    double norm()
    {
        return hypot(x, y);
    }

    // Returns angle between both vectors. Time: O(1).
    double angle(vector2d v)
    {
        return acos(dot(v) / (norm() * v.norm()));
    }

    // Returns if v is on the right side of this vector. Time: O(1).
    bool clockwise(vector2d v)
    {
        return cross(v) < -EPS;
    }

    // Returns if both vectors are collinear. Time: O(1).
    bool collinear(vector2d v)
    {
        return equals(cross(v), 0);
    }
};
