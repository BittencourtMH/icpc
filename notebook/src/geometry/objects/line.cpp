// Based on code by Halim (p. 297, 298, 299, 300).

#include "point.cpp"

struct line
{
    double a, b, c; // ax + by + c = 0
    point p1, p2; // ends of line segment

    // Returns if both lines are equal. Time: O(1).
    bool operator==(const line &l) const
    {
        return parallel(l) && equals(c, l.c);
    }

    // Returns if both lines are parallel. Time: O(1).
    bool parallel(line l) const
    {
        return equals(a, l.a) && equals(b, l.b);
    }

    // Given two non-parallel lines, returns the intersection point.
    // Time: O(1).
    point intersection(line l)
    {
        double x = (l.b * c - b * l.c) / (l.a * b - a * l.b);
        if (b > EPS)
            return {x, -a * x - c};
        return {x, -l.a * x - l.c};
    }

    // Returns the line's closest point to p. Time: O(1).
    point closest_point(point p)
    {
        vector2d v1 = p1.to_vector(p), v2 = p1.to_vector(p2);
        vector2d v = v2.scale(v1.dot(v2) / v2.dot(v2));
        return p1.translate(v.x, v.y);
    }

    // Returns the distance between line and p. Time: O(1).
    double distance(point p)
    {
        return p.distance(closest_point(p));
    }

    // Returns the line segment's closest point to p. Time: O(1).
    point segment_closest_point(point p)
    {
        vector2d v1 = p1.to_vector(p), v2 = p1.to_vector(p2);
        double s = v1.dot(v2) / v2.dot(v2);
        if (s <= 0)
            return p1;
        if (s >= 1)
            return p2;
        vector2d v = v2.scale(s);
        return p1.translate(v.x, v.y);
    }

    // Returns the distance between line segment and p. Time: O(1).
    double segment_distance(point p)
    {
        return p.distance(segment_closest_point(p));
    }
};

// Returns a line containing both points. Time: O(1).
line from_points(point p, point q)
{
    if (equals(p.x, q.x))
        return {1, 0, -p.x, p, q};
    double a = (q.y - p.y) / (p.x - q.x);
    return {a, 1, -a * p.x - p.y, p, q};
}
