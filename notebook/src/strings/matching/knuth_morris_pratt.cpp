// Based on code by Halim (p. 268).

#include "../../template.cpp"

const ll N = 1000000, M = 1000000; // size of text and pattern

char t[N + 1], p[M + 1]; // text and pattern
ll b[M]; // back table

// Computes the back table of a pattern of size m. Time: O(m).
void kmp_start(ll m)
{
    b[0] = -1;
    for (ll i = 0, j = -1; i < m; b[++i] = ++j)
        while (j >= 0 && p[i] != p[j])
            j = b[j];
}

// Returns if pattern, of size m, is found in text, of size n. Time:
// O(n + m).
bool kmp_find(ll n, ll m)
{
    for (ll i = 0, j = 0; i < n; i++, j++)
    {
        while (j >= 0 && t[i] != p[j])
            j = b[j];
        if (j == m - 1)
            return 1;
    }
    return 0;
}

// Returns the starting indices of all occurrences of pattern, of
// size m, in text, of size n. Time: O(n + m).
vi kmp_find_all(ll n, ll m)
{
    vi o;
    for (ll i = 0, j = 0; i < n; )
    {
        while (j >= 0 && t[i] != p[j])
            j = b[j];
        i++;
        j++;
        if (j == m)
            o.push_back(i - j), j = b[j];
    }
    return o;
}
