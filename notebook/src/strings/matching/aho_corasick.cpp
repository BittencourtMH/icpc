#include "../../template.cpp"

const ll N = 1e4 + 5; // number of text strings
const ll M = 1e6 + 5; // sum of sizes of text strings
const ll C = 26; // size of alphabet

ll f[M + 1], g[M + 1][C]; // fail and success automaton
string s[N]; // text strings
set<ll> mat[M + 1]; // list of matches per state

// Given n text strings that sum up to m characters, builds a
// matching automaton. Time: O(m).
void ac_build(ll n, ll m)
{
    for (ll i = 0; i <= m; i++)
        fill(g[i], g[i] + C, -1), mat[i].clear();
    fill(f, f + m + 1, -1);
    ll states = 1;
    for (ll i = 0; i < n; i++)
    {
        ll cur = 0;
        for (auto j: s[i])
        {
            ll c = j-'a';
            if(g[cur][c] == -1)
                g[cur][c] = states++;
            cur = g[cur][c];
        }
        mat[cur].insert(i);
    }
    for (ll c = 0; c < C; c++)
        if (g[0][c] == -1)
            g[0][c] = 0;
    queue<ll> q;
    for (ll c = 0; c < C; c++)
        if (g[0][c])
            f[g[0][c]] = 0, q.push(g[0][c]);
    while (!q.empty())
    {
        ll state = q.front();
        q.pop();
        for (ll c = 0; c < C; c++)
            if (g[state][c] != -1)
            {
                ll fail = f[state];
                while (g[fail][c] == -1)
                    fail = f[fail];
                fail = g[fail][c];
                f[g[state][c]] = fail;
                for (auto i: mat[fail])
                    mat[g[state][c]].insert(i);
                q.push(g[state][c]);
            }
    }
}

// Given a pattern p, of length n, returns the index of the first
// character of all m matches with all text strings. Time: O(n + m).
set<ll> ac_search(string &p)
{
    ll cur = 0;
    set<ll> match;
    for (auto i: p)
    {
        ll c = i-'a';
        while (g[cur][c] == -1)
            cur = f[cur];
        cur = g[cur][c];
        for (auto j: mat[cur])
            match.insert(j);
    }
    return match;
}
