// Based on code by Halim (p. 266).

#include "../../template.cpp"

// Given two strings t and p, of sizes n and m, returns the starting
// indices of all occurrences of p in t. Time: O(nm).
vi naive_matching(string &t, string &p)
{
    vi o;
    for (ll i = 0; i <= (ll)t.size() - (ll)p.size(); i++)
        for (ll j = 0; j < (ll)p.size(); j++)
        {
            if (t[i + j] != p[j])
                break;
            if (j == (ll)p.size() - 1)
                o.push_back(i);
        }
    return o;
}
