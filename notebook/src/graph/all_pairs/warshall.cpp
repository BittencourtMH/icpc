// Based on code by Halim (p. 184).

#include "../../template.cpp"

const ll N = 100; // maximum number of vertices

bool g[N][N]; // true if vertex j is reachable from i

// Given n vertices, stores transitive closure of all vertices. Time:
// O(n^3).
void warshall(ll n)
{
    for (ll i = 0; i < n; i++)
        for (ll j = 0; j < n; j++)
            for (ll k = 0; k < n; k++)
                g[j][k] |= g[j][i] & g[i][k];
}
