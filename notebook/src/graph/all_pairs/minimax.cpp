// Based on code by Halim (p. 184).

#include "../../template.cpp"

const ll N = 100; // maximum number of vertices

ll g[N][N]; // graph (adjacency matrix)

// Given n vertices, stores the minimum of maximum edge weight among
// all paths between all pairs of vertices. Time: O(n^3).
void minimax(ll n)
{
    for (ll i = 0; i < n; i++)
        for (ll j = 0; j < n; j++)
            for (ll k = 0; k < n; k++)
                g[j][k] = min(g[j][k], max(g[j][i], g[i][k]));
}
