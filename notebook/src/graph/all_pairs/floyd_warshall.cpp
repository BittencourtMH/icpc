// Based on code by Halim (p. 180).

#include "../../template.cpp"

const ll N = 100; // maximum number of vertices

ll g[N][N]; // graph (adjacency matrix)

// Given n vertices, stores the distance of all pairs of vertices.
// Time: O(n^3).
void floyd_warshall(ll n)
{
    for (ll i = 0; i < n; i++)
        for (ll j = 0; j < n; j++)
            for (ll k = 0; k < n; k++)
                g[j][k] = min(g[j][k], g[j][i] + g[i][k]);
}
