// Based on code by Halim (p. 200-201).

#include "../../template.cpp"

const ll N = 10000000; // maximum number of vertices

vi g[N]; // tree (adjacency list)
ll memo[N][2]; // memoization table

// Given n vertices and m edges, returns the size of minimum vertex
// cover starting at vertex u. Flag s is true if vertex u is
// selected. Time: O(n + m).
ll dfs(ll u, bool s)
{
    if (memo[u][s] != -1)
        return memo[u][s];
    ll ans = 0;
    if (s)
        for (auto v: g[u])
            ans = min(dfs(v, 1), dfs(v, 0));
    else
        for (auto v: g[u])
            ans += dfs(v, 1);
    return memo[u][s] = ans;
}

// Given n vertices and m edges, returns the size of minimum vertex
// cover starting at root vertex r. Time: O(n + m).
ll mvc(ll r, ll n)
{
    for (ll i = 0; i < n; i++)
        memo[i][0] = memo[i][1] = -1;
    return min(dfs(r, 1), dfs(r, 0));
}
