#include "../../template.cpp"

const ll N = 1e4 + 5; // number of vertices (including nil)

ll sa, sb; // size of sets A and B
ll nil = N - 1; // pair of unmatched vertices
ll ma[N], mb[N]; // pair of matched vertices of sets A and B
ll dist[N]; // distance to specific vertex
vi g[N]; // graph (adjacency list)

// Given a graph with V vertices and E edges, runs a breadth-first
// search. Time: O(V + E).
bool bfs()
{
    queue<ll> q;
    for (ll i = 0; i < sa; i++)
        if (ma[i] == nil)
            dist[i] = 0, q.push(i);
        else
            dist[i] = INT_MAX;
    dist[nil] = INT_MAX;
    while (!q.empty())
    {
        ll u = q.front();
        q.pop();
        if (dist[u] < dist[nil])
            for (auto v: g[u])
                if (dist[mb[v]] == INT_MAX)
                    dist[mb[v]] = dist[u] + 1, q.push(mb[v]);
    }
    return dist[nil] != INT_MAX;
}

// Given a graph with E edges, runs a depth-first search starting at
// vertex u. Time: O(E).
bool dfs(ll u)
{
    if (u == nil)
        return 1;
    for (auto v: g[u])
        if (dist[mb[v]] == dist[u] + 1 && dfs(mb[v]))
        {
            mb[v] = u;
            ma[u] = v;
            return 1;
        }
    dist[u] = INT_MAX;
    return 0;
}

// Given a graph with V vertices and E edges, returns the number of
// matches of the maximum cardinality bipartite matching. Time:
// O(sqrt V * E).
ll hopcroft_karp()
{
    ll matches = 0;
    fill(ma, ma + sa, nil);
    fill(mb, mb + sb, nil);
    while (bfs())
        for (ll i = 0; i < sa; i++)
            if (ma[i] == nil && dfs(i))
                matches++;
    return matches;
}
