// Based on text by Halim (p. 204).

#include "../../template.cpp"

const ll N = 10000000; // maximum number of vertices

vi g[N]; // graph (adjacency list)

// Given n vertices, returns true if the graph has an Euler tour.
// Time: O(n).
bool euler_tour(ll n)
{
    for (ll i = 0; i < n; i++)
        if (g[i].size() % 2)
            return 0;
    return 1;
}

// Given n vertices, returns true if the graph has an Euler path.
// Time: O(n).
bool euler_path(ll n)
{
    ll odd = 0;
    for (ll i = 0; i < n; i++)
        if (g[i].size() % 2)
            odd++;
    return odd == 2;
}
