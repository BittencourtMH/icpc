// Based on code by Halim (p. 209).

#include "../../template.cpp"

const ll N = 100; // maximum number of vertices

vi g[N]; // graph (adjacency list)
bool vis[N]; // true if vertex i was visited
ll mat[N]; // vertex matched with i

// Given n vertices and m edges, returns true if there is an
// augmenting path. Time: O(n + m).
bool dfs(ll u)
{
    if (vis[u])
        return 0;
    vis[u] = 1;
    for (auto v: g[u])
        if (mat[v] == -1 || dfs(mat[v]))
        {
            mat[v] = u;
            return 1;
        }
    return 0;
}

// Given n vertices and m edges, returns the maximum number of
// matchings. Time: O(nm).
ll mcbm(ll n)
{
    ll match = 0;
    fill(mat, mat + n, -1);
    for (ll i = 0; i < n; i++)
    {
        fill(vis, vis + n, 0);
        match += dfs(i);
    }
    return match;
}
