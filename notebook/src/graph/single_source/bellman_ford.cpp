// Based on code by Halim (p. 176, 177).

#include "../../template.cpp"

const ll N = 10000; // maximum number of vertices

vii g[N]; // graph (adjacency list) - {vertex, weight}
ll dist[N]; // distance from source vertex

// Given n vertices and m edges, stores the distance of all vertices
// to source s. Time: O(mn).
void bellman_ford(ll n, ll s)
{
    fill(dist, dist + n, INT_MAX);
    dist[s] = 0;
    for (ll i = 0; i < n - 1; i++)
        for (ll j = 0; j < n; j++)
            for (auto p: g[j])
            {
                ll u = p.first, d = p.second;
                dist[u] = min(dist[u], dist[j] + d);
            }
}

// Given n vertices, m edges and a source vertex s, returns true if
// the graph has a negative cycle. Time: O(mn).
bool negative_cycle(ll n, ll s)
{
    bellman_ford(n, s);
    for (ll i = 0; i < n; i++)
        for (auto p: g[i])
        {
            ll u = p.first, d = p.second;
            if (dist[i] + d < dist[u])
                return 1;
        }
    return 0;
}
