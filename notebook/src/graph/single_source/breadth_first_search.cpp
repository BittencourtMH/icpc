// Based on code by Halim (p. 171-172).

#include "../../template.cpp"

const ll N = 10000; // maximum number of vertices

vi g[N]; // graph (adjacency list)
ll dist[N]; // distance from source vertex

// Given n vertices and m edges, runs a breadth-first search from
// source s and stores the distance of all vertices to s. Time:
// O(n + m).
void bfs(ll n, ll s)
{
    queue<ll> q;
    q.push(s);
    fill(dist, dist + n, INT_MAX);
    dist[s] = 0;
    while (!q.empty())
    {
        ll u = q.front();
        q.pop();
        for (auto v: g[u])
            if (dist[v] == INT_MAX)
                dist[v] = dist[u] + 1, q.push(v);
    }
}
