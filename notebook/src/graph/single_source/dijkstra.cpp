// Based on code by Halim (p. 173).

#include "../../template.cpp"

const ll N = 10000; // maximum number of vertices

vii g[N]; // graph (adjacency list) - {vertex, weight}
ll dist[N]; // distance from source vertex

// Given n vertices and m edges, stores the distance of all vertices
// to source s. Time: O((n + m) log n).
void dijkstra(ll n, ll s)
{
    fill(dist, dist + n, INT_MAX);
    priority_queue<ii, vii, greater<ii> > pq;
    pq.push({0, s});
    dist[s] = 0;
    while (!pq.empty())
    {
        ii p = pq.top();
        pq.pop();
        ll u = p.second, d = p.first;
        if (d > dist[u])
            continue;
        for (auto q: g[u])
        {
            ll v = q.first, w = q.second;
            if (d + w < dist[v])
                dist[v] = d + w, pq.push({dist[v], v});
        }
    }
}
