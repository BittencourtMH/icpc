// Based on code by Halim (p. 164).

#include "../../structures/union_find_disjoint_set.cpp"

vector<pair<ll, ii> > edges; // graph - {weight, vertices}
ufds uf; // components of graph

// Given n vertices and m edges, returns cost of minimum spanning
// tree. Time: O(m log n).
ll kruskal(ll n)
{
    sort(edges.begin(), edges.end());
    uf.start(n);
    ll c = 0;
    for (auto e: edges)
    {
        ll u = e.second.first, v = e.second.second, w = e.first;
        if (uf.find(u) != uf.find(v))
            c += w, uf.merge(u, v);
    }
    return c;
}
