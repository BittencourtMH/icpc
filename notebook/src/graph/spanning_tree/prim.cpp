// Based on code by Halim (p. 165).

#include "../../template.cpp"

const ll N = 10000; // maximum number of vertices

vii g[N]; // graph (adjacency list) - {vertex, weight}
bool visit[N]; // true if vertex i is in spanning tree

// Given n vertices and m edges, returns cost of minimum spanning
// tree. Time: O(m log n).
ll prim(ll n)
{
    fill(visit, visit + n, 0);
    priority_queue<ii, vector<ii>, greater<ii> > pq;
    pq.push({0, 0});
    ll c = 0;
    while (!pq.empty())
    {
        ii p = pq.top();
        pq.pop();
        ll w = p.first, u = p.second;
        if (!visit[u])
        {
            c += w;
            visit[u] = 1;
            for (auto q: g[u])
            {
                ll v = q.first, x = q.second;
                if (!visit[v])
                    pq.push({x, v});
            }
        }
    }
    return c;
}
