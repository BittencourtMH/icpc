// Based on code by Halim (p. 157-158).

#include "../../template.cpp"

const ll N = 10000; // maximum number of vertices

vi g[N]; // graph (adjacency list)
vii brid; // bridges (edge list)
ll num[N]; // number of vertex i
ll low[N]; // number of lowest vertex reachable from i
bool art[N]; // true if vertex i is an articulation point
ll vert; // number of visited vertices
ll child; // number of root children

// Given n vertices and m edges, runs a depth-first search starting
// at vertex u, whose parent is p. Vertex r is the root. Time:
// O(n + m).
void dfs(ll u, ll p, ll r)
{
    num[u] = low[u] = ++vert;
    for (auto v: g[u])
        if (!num[v])
        {
            if (u == r)
                child++;
            dfs(v, u, r);
            if (low[v] >= num[u])
                art[u] = 1;
            if (low[v] > num[u])
                brid.push_back({u, v});
            low[u] = min(low[u], low[v]);
        }
        else if (v == p)
            low[u] = min(low[u], low[v]);
}

// Given n vertices and m edges, stores articulation points and
// bridges. Time: O(n + m).
void articulation_points_bridges(ll n)
{
    fill(num, num + n, 0);
    fill(low, low + n, 0);
    vert = 0;
    for (ll i = 0; i < n; i++)
        if (!num[i])
            child = 0, dfs(i, i, i), art[i] = child > 1;
}
