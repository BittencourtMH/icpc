// Based on code by Halim (p. 149).

#include "../../template.cpp"

const ll N = 10000; // maximum number of vertices

vi g[N]; // graph (adjacency list)
bool visit[N]; // visited vertices

// Given n vertices and m edges, runs a breadth-first search starting
// at vertex u. Time: O(n + m).
void bfs(ll u)
{
    queue<ll> q;
    visit[u] = 1;
    q.push(u);
    while (!q.empty())
    {
        u = q.front();
        q.pop();
        for (auto v: g[u])
            if (!visit[v])
                visit[v] = 1, q.push(v);
    }
}
