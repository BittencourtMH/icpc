// Based on code by Halim (p. 150).

#include "breadth_first_search.cpp"

// Given n vertices and m edges, returns the number of connected
// components. Time: O(mn).
ll connected_components(ll n)
{
    ll cc = 0;
    for (ll i = 0; i < n; i++)
        if (!visit[i])
            bfs(i), cc++;
    return cc;
}
