// Based on code by Halim (p. 153).

#include "../../template.cpp"

const ll N = 10000; // maximum number of vertices

vi g[N]; // graph (adjacency list)
char col[N]; // color of vertices

// Given n vertices, m edges and a starting vertex u, returns true if
// the subgraph is bipartite. Time: O(n + m).
bool dfs(ll u)
{
    for (auto v: g[u])
        if (col[v] == -1)
        {
            col[v] = 1 - col[u];
            if (!dfs(v))
                return 0;
        }
        else if (col[u] == col[v])
            return 0;
    return 1;
}

// Given n vertices and m edges, returns true if the graph is
// bipartite. Time: O(n + m).
bool bipartite(ll n)
{
    fill(col, col + n, -1);
    for (ll i = 0; i < n; i++)
        if (col[i] == -1)
        {
            col[i] = 0;
            if (!dfs(i))
                return 0;
        }
    return 1;
}
