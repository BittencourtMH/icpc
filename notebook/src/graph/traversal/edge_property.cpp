// Based on code by Halim (p. 154-155).

#include "../../template.cpp"

const ll N = 10000; // maximum number of vertices

enum vertex_state
{
    UNVISITED, VISITING, VISITED
};
enum edge_type
{
    TREE, BACK, TWO_WAY, FORWARD
};
struct edge
{
    ll u, v; // edge from u to v
    edge_type t; // type according to the depth-first search
};

vi g[N]; // graph (adjacency list)
vector<edge> edges; // graph (edge list)
vertex_state vertices[N]; // state of vertex in depth-first search

// Given n vertices and m edges, classifies vertices according to
// their property. Runs a depth-first search starting at vertex u,
// whose parent is p. Time: O(n + m).
void edge_property(ll u, ll p)
{
    vertices[u] = VISITING;
    for (auto v : g[u])
        if (vertices[v] == UNVISITED)
        {
            edges.push_back({u, v, TREE});
            edge_property(v, u);
        }
        else if (vertices[v] == VISITING)
            edges.push_back({u, v, v == p ? TWO_WAY : BACK});
        else
            edges.push_back({u, v, FORWARD});
    vertices[u] = VISITED;
}
