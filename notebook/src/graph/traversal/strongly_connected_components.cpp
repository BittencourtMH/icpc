// Based on code by Halim (p. 159).

#include "../../template.cpp"

const ll N = 10000; // maximum number of vertices

vi g[N]; // graph (adjacency list)
ll num[N]; // number of vertex i
ll low[N]; // number of lowest vertex reachable from i
bool visit[N]; // true if vertex i is being visited
vvi sccs; // list of strongly connected components
vi comp; // current component
ll vert; // number of visited vertices

// Given n vertices and m edges, runs a depth-first search starting
// at vertex u. Time: O(n + m).
void dfs(ll u)
{
    num[u] = low[u] = ++vert;
    comp.push_back(u);
    visit[u] = 1;
    for (auto v : g[u])
    {
        if (!num[v])
            dfs(v);
        if (visit[v])
            low[u] = min(low[u], low[v]);
    }
    if (low[u] == num[u])
    {
        vi n_scc;
        while (1)
        {
            ll v = comp.back();
            comp.pop_back();
            visit[v] = 0;
            n_scc.push_back(v);
            if (u == v)
                break;
        }
        sccs.push_back(n_scc);
    }
}

// Given n vertices and m edges, stores strongly connected
// components. Time: O(n + m).
void scc(ll n)
{
    fill(num, num + n, 0);
    fill(low, low + n, 0);
    fill(visit, visit + n, 0);
    vert = 0;
    sccs.clear();
    for (ll i = 0; i < n; i++)
        if (!num[i])
            dfs(i);
}
