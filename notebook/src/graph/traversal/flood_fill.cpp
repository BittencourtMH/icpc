// Based on code by Halim (p. 150).

#include "../../template.cpp"

const ll M = 10000, N = 10000; // maximum number of rows and columns

char g[M][N]; // 2D grid
ll dx[] = {-1, 0, 0, 1, -1, -1, 1, 1}; // delta for rows
ll dy[] = {0, -1, 1, 0, -1, 1, -1, 1}; // delta for columns

// Given a 2D grid with m rows and n columns, a starting point (x, y)
// and symbols (unvis, vis) for (unvisited, visited) cells, returns
// the number of filled cells after a w-way flood fill. Time: O(mn).
ll flood_fill(ll m, ll n, ll x, ll y, ll w, char unvis, char vis)
{
    if (x < 0 || x >= m || y < 0 || y >= n || g[x][y] != unvis)
        return 0;
    ll s = 1;
    g[x][y] = vis;
    for (ll i = 0; i < w; i++)
        s += flood_fill(m, n, x + dx[i], y + dy[i], w, unvis, vis);
    return s;
}
