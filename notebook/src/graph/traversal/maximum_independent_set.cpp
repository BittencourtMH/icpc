// Based on code by Halim (p. 328).

#include "../../template.cpp"

const ll N = 10; // maximum number of vertices

ll g[N]; // graph (bitwise adjacency matrix)
ll n; // number of vertices
ll maximum = 0; // size of maximum independent set

// Given n vertices, m edges, a starting vertex u, a set of used
// vertices and size of independent set so far, stores the size of
// maximum independent set. Time: O(n!)
void mis(ll u, ll used, ll d)
{
    if (used == (1 << n) - 1)
        maximum = max(maximum, d);
    else
        for (ll i = u; i < n; i++)
            if (!(used & (1 << i)))
                mis(i + 1, used | g[i], d + 1);
}
