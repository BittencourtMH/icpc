// Based on code by Halim (p. 147).

#include "../../template.cpp"

const ll N = 10000; // maximum number of vertices

vi g[N]; // graph (adjacency list)
bool visit[N]; // visited vertices

// Given n vertices and m edges, runs a depth-first search starting
// at vertex u. Time: O(n + m).
void dfs(ll u)
{
    visit[u] = 1;
    for (auto v: g[u])
        if (!visit[v])
            dfs(v);
}
