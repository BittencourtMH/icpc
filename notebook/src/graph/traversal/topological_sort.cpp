// Based on code by Halim (p. 151-152).

#include "../../template.cpp"

const ll N = 10000; // maximum number of vertices

vi g[N]; // graph (adjacency list)
bool visit[N]; // visited vertices
vi ts; // vertices in reverse order

// Given n vertices and m edges, and a starting vertex u, stores
// graph in reverse topological order. Time: O(n + m).
void topological_sort(ll u)
{
    visit[u] = 1;
    for (auto v: g[u])
        if (!visit[v])
            topological_sort(v);
    ts.push_back(u);
}
