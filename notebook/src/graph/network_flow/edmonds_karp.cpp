// Based on code by Halim (p. 190).

#include "../../template.cpp"

const ll N = 10; // maximum number of vertices

ll g[N][N]; // residual graph (adjacency matrix)
ll p[N]; // parent of vertex i
ll s = N - 2; // number of source vertex
ll t = N - 1; // number of sink vertex

// Given n vertices and m edges, runs a depth-first search starting
// at vertex u with flow f and returns the minimum flow in the path.
// Time: O(m + n).
ll dfs(ll u, ll f)
{
    if (u != s)
    {
        if (p[u] == -1)
            return 0;
        f = dfs(p[u], min(f, g[p[u]][u]));
        g[p[u]][u] -= f;
        g[u][p[u]] += f;
    }
    return f;
}

// Given n vertices and m edges, returns the maximum flow from s to
// t. Time: O(n ^ 3 * m).
ll edmonds_karp(ll n)
{
    ll mf = 0;
    while (1)
    {
        fill(p, p + n, -1);
        queue<ll> q;
        q.push(s);
        while (!q.empty())
        {
            ll u = q.front();
            q.pop();
            if (u == t)
                break;
            for (ll i = 0; i < n; i++)
                if (g[u][i] > 0 && p[i] == -1)
                    p[i] = u, q.push(i);
        }
        ll f = dfs(t, INT_MAX);
        if (!f)
            break;
        mf += f;
    }
    return mf;
}
